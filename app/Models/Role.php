<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use App\Models\Permission;

class Role extends Model
{
    public function permissions()
    {
        return $this->belongsToMany(Permission::class, 'permission_role', 'permission', 'role');
    }

    public function users()
    {
        return $this->belongsToMany(User::class, 'role_user', 'role', 'user');
    }
}
