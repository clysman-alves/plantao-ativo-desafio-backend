<?php

namespace App\Models;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'user_id', 'title', 'content'
    ];

    public function author(){
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function tags(){
        return $this->morphToMany(Tag::class, 'taggable');
    }

    public function format(){
        return [
            'id' => $this->id,
            'title' => $this->title,
            'author' => $this->author->name,
            'content' => $this->content,
            'tags' => collect($this->tags)->map(function($item) {
                return $item->title;
            }),
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
