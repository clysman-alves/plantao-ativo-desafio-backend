<?php

namespace App\Traits;

use Illuminate\Http\JsonResponse;

trait HttpResponse
{
    /**
     * Success Response
     *
     * @param  array  $data
     * @param  int  $status
     * @param  string  $message
     * @return JsonResponse
     */
    protected function success($message, $data = [], $status = 200)
    {
        return response()->json([
            'success' => true,
            'data' => $data,
            'message' => $message,
        ], $status);
    }

    /**
     * Failure Response
     *
     * @param  int  $apiCode
     * @param  int  $status
     * @param  string  $message
     * @return JsonResponse
     */
    protected function failure($message, $apiCode = null, $status = 422)
    {
        return response([
            'success' => false,
            'message' => $message,
            'code' => $apiCode ?? $status
        ], $status);
    }
    /**
     * NoContent Response
     *
     * @return JsonResponse
     */
    protected function noContent()
    {
        return response()->noContent();
    }
}
