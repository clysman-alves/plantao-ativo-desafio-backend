<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Contracts\PostRepositoryContract;
use App\Repositories\Contracts\TagRepositoryContract;

class HomeController extends Controller
{
    private $post;
    private $tag;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(
        PostRepositoryContract $postRepository,
        TagRepositoryContract $tagRepository
    )
    {
        $this->middleware('auth');
        $this->post = $postRepository;
        $this->tag = $tagRepository;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $posts = $this->post;
        $tags = $this->tag->all();

        if($request->get('tag')) {
            $posts = $this->post->findPostsByTagName($request->get('tag'));
        } else {
            $posts = $this->post->all();
        }

        return view('home')->with([
            'posts' => $posts,
            'tags' => $tags,
            'featuredPost' => $posts->random()
        ]);
    }
}
