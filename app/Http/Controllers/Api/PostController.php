<?php

namespace App\Http\Controllers\Api;

use App\Models\Post;
use App\Api\ApiError;
use App\Repositories\Contracts\PostRepositoryContract;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\PostRequest;
use App\Traits\HttpResponse;

class PostController extends Controller
{
    use HttpResponse;

    private $post;

    public function __construct(PostRepositoryContract $postRepository){
        $this->post = $postRepository;
        $this->middleware('auth:api', ['except' => ['edit']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $posts = $this->post;

        if($request->get('tag')) {
            $posts = $this->post->findPostsByTagName($request->get('tag'));
        } else {
            $posts = $this->post->all();
        }

        return $this->success('See all posts', $posts->map->format(), 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostRequest $request)
    {
        try {
            $post = $this->post->create([
                "title" => $request->title,
                "content" => $request->content,
                "user_id" => auth()->user()->id
            ]);

            $post->tags()->sync($request->tags);

            return $this->success(
                'Post created',
                $this->post->findByID($post->id)->format(),
                201
            );

        } catch (\Exception $e) {
            if(config('app.debug')){
                return $this->failure($e->getMessage(), 1010, 500);
            }
            return $this->failure('Error in post creating', 1021, 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = $this->post->findById($id)->format();

        return $this->success('See post', $post, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = $this->post->findById($id);

        $this->authorize('update-post', $post);

        return view('/post/edit')->with('post', $post);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PostRequest $request, $id)
    {
        try {
            $post =  $this->post->update([
                "title" => $request->title,
                "content" => $request->content
            ], $id);

            $post->tags()->sync($request->tags);

            return $this->success(
                'Post updated',
                $post->format(),
                200
            );
        } catch (\Exception $e) {
            if(config('app.debug')){
                return $this->failure($e->getMessage(), 1010, 500);
            }
            return $this->failure('Error in post update', 1023, 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $this->post
                ->findById($id)
                ->tags()
                ->detach();

            $this->post
                ->delete($id);

            return $this->noContent();
        } catch (\Exception $e) {
            if(config('app.debug')){
                return $this->failure($e->getMessage(), 1010, 500);
            }
            return $this->failure('Error in post delete', 1023, 500);
        }
    }
}
