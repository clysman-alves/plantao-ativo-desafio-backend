<?php

namespace App\Repositories\Contracts;

interface UserRepositoryContract
{
    public function all();

    public function findById($id);

    public function findUserByEmail($email);

    public function create(array $data);

    public function firstOrCreate(array $checkParam ,array $data);

    public function update(array $data, $id);

    public function delete($id);
}
