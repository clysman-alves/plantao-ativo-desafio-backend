<?php

namespace App\Repositories\Contracts;

interface PostRepositoryContract
{
    public function all();

    public function findById($id);

    public function findPostsByTagName($tag);

    public function create(array $data);

    public function update(array $data, $id);

    public function delete($id);
}
