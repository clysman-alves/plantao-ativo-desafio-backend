<?php

namespace App\Repositories\Eloquent;

use App\Models\Tag;
use Illuminate\Http\Request;
use App\Repositories\Contracts\TagRepositoryContract;

class TagRepository extends AbstractRepository implements TagRepositoryContract
{
    protected $model;

    public function __construct(Tag $model)
    {
        $this->model = $model;
    }
}
