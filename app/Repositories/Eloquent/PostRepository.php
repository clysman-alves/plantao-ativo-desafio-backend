<?php

namespace App\Repositories\Eloquent;

use App\Models\Post;
use App\Repositories\Contracts\PostRepositoryContract;

class PostRepository extends AbstractRepository implements PostRepositoryContract
{
    protected $model;

    public function __construct(Post $model)
    {
        $this->model = $model->with('author', 'tags');
    }

    public function findPostsByTagName($tag)
    {
        return $this->model->whereHas(
            'tags', function ($query) use ($tag) {
                return $query->where('title', $tag);
            }
        )->get();
    }
}
