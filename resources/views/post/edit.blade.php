@extends('layouts.app')

@section('content')
    <div class="container">
        <form action="{{ route('api.posts.update', ['id' => $post->id]) }}" method="POST">
            @method('PUT')
            @csrf()
            <div class="form-group">
                <label for="title">Title</label>
                <input type="text" name="title" class="form-control" id="title" value="{{ $post->title }}" placeholder="Title">
            </div>
            <div class="form-group">
                <label for="content">Content</label>
                <textarea class="form-control" name="content" id="content" rows="3" placeholder="Contet" required>{{ $post->content }}</textarea>
            </div>
            <button class="btn btn-lg btn-primary btn-block" type="submit">Editar</button>
        </form>
    </div>
@endsection
