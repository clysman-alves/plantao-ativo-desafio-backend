@extends('layouts.app')

@section('content')
<div class="container">
    <div class="nav-scroller py-1 mb-2">
        <nav class="nav d-flex justify-content-between">
            @forelse($tags as $tag)
                <a class="p-2 text-muted" href="{!! '?tag='.$tag->title !!}">{{ $tag->title }}</a>
            @empty
                <h4>Nenhuma tag encontrada</h4>
            @endforelse
        </nav>
    </div>
    <div class="jumbotron p-3 p-md-5 text-white rounded bg-dark">
        <div class="col-md-6 px-0">
          <h1 class="display-4 font-italic">{{ $featuredPost->title }}</h1>
          <p class="lead my-3">{{ $featuredPost->content }}</p>
          <p class="lead mb-0"><a href="#" class="text-white font-weight-bold">Continue reading...</a></p>
        </div>
    </div>
    @can('VIEW_POST')
    <div class="row mb-2">
        @forelse($posts as $post)
            <div class="col-md-6">
                <div class="card flex-md-row mb-4 box-shadow h-md-250">
                    <div class="card-body d-flex flex-column align-items-start">
                    <strong class="d-inline-block mb-2 text-primary">{{
                            $post->tags->map(
                                function($item){
                                    return $item->title;
                                }
                            )->implode(' ')
                        }}
                    </strong>
                    <h3 class="mb-0">
                        <a class="text-dark" href="#">{{ (strlen($post->title) > 24) ? substr($post->title, 0, 21).'...' : $post->title }}</a>
                    </h3>
                    <div class="mb-1 text-muted">{{ $post->created_at }}</div>
                    <p class="card-text mb-auto">{{ (strlen($post->content) > 135) ? substr($post->content ,0, 132).'...' : $post->content }}</p>
                    <a href="#">Continue reading</a>
                    @can('update-post', $post)
                    <div class="d-flex mt-2">
                        <form action="{{ route('api.posts.delete', ['id' => $post->id]) }}" method="post">
                            @csrf()
                            @method('DELETE')
                            <button class="btn btn-danger" type="submit">
                                Apagar
                            </button>
                        </form>

                        <a class="btn btn-primary ml-2 mr-2" href="{{ route('web.posts.edit', ['id' => $post->id]) }}">
                            Editar
                        </a>
                    </div>
                    @endcan
                    </div>
                    <img class="card-img-right flex-auto d-none d-md-block" data-src="holder.js/200x250?theme=thumb" alt="Thumbnail [200x250]" style="width: 200px; height: 250px;" src="https://images.unsplash.com/photo-1481627834876-b7833e8f5570?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&auto=format&fit=crop&w=541&q=80" data-holder-rendered="true">
                </div>
            </div>
        @empty
            <div class="col-md-6 d-flex justify-content-center">
                <h1>Nenhum post encontrado</h1>
            </div>
        @endforelse
    </div>
    @endcan
</div>
@endsection
