<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::namespace('Api')->name('api.')->group(function(){
    Route::prefix('posts')->name('posts.')->group(function(){
        Route::get('/', 'PostController@index')->name('all');
        Route::get('/{id}', 'PostController@show')->name('show');
        Route::post('/', 'PostController@store')->name('store');
        Route::put('/{id}', 'PostController@update')->name('update');
        Route::delete('/{id}', 'PostController@destroy')->name('delete');
    });

    Route::prefix('auth')->name('auth.')->group( function ($router) {
        Route::post('login', 'AuthController@login')->name('login');
        Route::post('logout', 'AuthController@logout')->name('logout');
        Route::post('refresh', 'AuthController@refresh')->name('refresh');
        Route::post('me', 'AuthController@me')->name('me');

    });
});

Route::namespace('Auth')->middleware('web')->name('api.')->group(function(){
    Route::prefix('oauth')->name('social.')->group(function(){
        Route::get('login/{provider}', 'LoginController@redirectToProvider')->name('login');
        Route::get('login/{provider}/callback', 'LoginController@handleProviderCallback')->name('callback');
    });
});


