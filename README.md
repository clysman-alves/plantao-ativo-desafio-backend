# Plantão Ativo API Desafio

| Informações |
|:------------:|
| Esta REST API foi desenvolvida em resposta ao desafio proposto pelo [Plantão Ativo](https://plantaoativo.com/). Sua principal funcionalidade é ser um gerenciador de postagens com seus respectivos títulos, autores, conteúdos e tags.

## Documentação
Para ser redirecionado a documentação clique [aqui](https://plantaoativo-desafio.herokuapp.com/documentation).

## Instalação
## Pré-requesitos

[PHP](https://www.php.net/downloads.php) >= 7.2.5<br>
[Composer](https://getcomposer.org/download/)<br>
[MySQL](https://www.mysql.com/downloads/) >= 5.6<br>

## Projeto
**1** - Faça o download do repositório

``
$ git clone https://clysman-alves@bitbucket.org/clysman-alves/plantao-ativo-desafio-backend.git
``

**2**  - Na pasta do projeto faça a cópia do arquivo `.env.example` e renomeie-o para `.env`

``
$ cp .env.example .env
``

**3**  - No arquivo `.env` configure as variáveis de ambiente da aplicação com suas informações

```js
   APP_NAME=Laravel
   APP_ENV=local
   APP_KEY=
   APP_DEBUG=true
   APP_URL=http://localhost

   DB_CONNECTION=mysql
   DB_HOST=127.0.0.1
   DB_PORT=3306
   DB_DATABASE=laravel
   DB_USERNAME=root
   DB_PASSWORD=

   FACEBOOK_CLIENT_ID=
   FACEBOOK_CLIENT_SECRET=
   FACEBOOK_CALLBACK_URL=http://localhost:8000/api/login/facebook/callback

   GITHUB_CLIENT_ID=
   GITHUB_CLIENT_SECRET=
   GITHUB_CALLBACK_URL=http://localhost:8000/api/login/github/callback
```

**4**  - Baixe os pacotes necessários para inicialização do projeto

``
$ composer install
``

**5**  - Gere a chave da aplicação

``
$ php artisan key:generate
``

**6**  - Suba as Migrations para o banco de dados

``
$ php artisan migrate --seed
``

`Nota:` O comando acima também irá fazer o povoamento do banco de dados com informações, caso não queira remova o paramêtro `--seed`
**7**  - Gere o secret utilizado pelo JWT

``
$ php artisan jwt:secret
``

**8**  - Adicione a chave de ativação do ACL no arquivo .env
```
  ACL_ENABLE=true
```

**9**  - Inicie o servidor no seu ambiente local

``
$ php artisan serve
``

**10**  - Abra seu navegador e acesse: http://127.0.0.1:8000/

## Informações Sobre a API
Os Endpoints suportados pela API são:

`POST`

| Endpoints      | Descrição                       |
|:--------------|:----------------------------------|
| `/login` | Realiza a autenticação do usuário e redireciona para home.
| `/logout` | Encerra a sessão do usuário via web.
| `/register` | Realiza o registro de um novo usuário.
| `/api/auth/login` | Realiza a autenticação e gerar o token de acesso.
| `/api/auth/logout` | Encerra e invalida o token gerado.
| `/api/auth/me`| Verifica qual usuário está atribuido ao token passado.
| `/api/auth/refresh` | Realiza o registro de um novo usuário.
| `/api/posts` | Realiza o registro de um novo post.

`GET`

| Endpoints      | Descrição                       |
|:--------------|:----------------------------------|
| `/api/posts`      | Retorna a lista de todos os posts

`POST`

| Endpoints      | Descrição                       |
|:--------------|:----------------------------------|
| `/api/posts/`      | Realiza a criação de um novo post

`GET /{id}`

| Endpoints      | Descrição                       |
|:--------------|:----------------------------------|
| `/api/posts/{id}`      | Retorna as informações de um post específico

`GET /{provider}`

| Endpoints      | Descrição                       |
|:--------------|:----------------------------------|
| `/api/oauth/login/{provider} `      | Realiza o redirecionamento para o provider indicado.
| `/api/oauth/login/{provider}/callback`    | Persiste as informações vindas do provider.

`PUT /{id}`

| Endpoints      | Descrição                       |
|:--------------|:----------------------------------|
| `/api/posts/{id}`      | Realiza a edição de um post específico

`DELETE /{id}`

| Endpoints      | Descrição                       |
|:--------------|:----------------------------------|
| `/api/posts/{id}`      | Realiza a remoção de um post específico

## Parâmetros

| parâmetro                    | descrição                 |  | |
|:-----------------------------|:----------------------------|:----------------------------|:----------------------------|
| `tag`| Filtras os posts pelas tags | string | opcional


## Exemplos
Abaixo estão alguns exemplos de como realizar o uso dos Endpoints

## Listando todos os posts

### Request

`GET /api/posts?clientId={client_id}&tag=node&page=1`


### Response

  **Headers**
  
    Content-Type: application/json
    
  **Body**
  
```js
		{
		  "success": true,
		  "data": [
			  {
				"id": 32,
				"title": "Notion",
				"author": "Marcia Thiel",
				"content": "Sed soluta nemo et consectetur reprehenderit ea reprehenderit sit.",
				"tags": [
				  "organization",
				  "planning",
				  "collaboration"
				],
				"created_at": "2021-03-20T11:34:12.000000Z",
				"updated_at": "2021-03-20T11:34:12.000000Z"
			  }
		  ],
		  "message": "Post updated"
		}
```

    
## Pegando um post específico

### Request

`GET /api/posts/{id}`


### Response

  **Headers**
  
    Content-Type: application/json
    
  **Body**

```js
		{
		  "success": true,
		  "data": {
			"id": 32,
			"title": "Notion",
			"author": "Marcia Thiel",
			"content": "Sed soluta nemo et consectetur reprehenderit ea reprehenderit sit.",
			"tags": [
			  "organization",
			  "planning",
			  "collaboration"
			],
			"created_at": "2021-03-20T11:34:12.000000Z",
			"updated_at": "2021-03-20T11:34:12.000000Z"
		  },
		  "message": "See post"
		}
```    

## Criando um novo post

### Request

`POST /api/posts`

  **Headers**
  
    Authentication: Bearer JWT
    Accept: application/json
    Content-Type: application/json
    
  **Body**
```js
    {
      "author": "Marcia Thiel",
      "title": "Notion",
      "content": "Sed soluta nemo et consectetur reprehenderit ea reprehenderit sit.",
      "tags": [1,2,3]
    }
```
    

### Response

  **Headers**

    Content-Type: application/json
  **Body**
  
```js
		{
		  "success": true,
		  "data": {
			"id": 32,
			"title": "Notion",
			"author": "Marcia Thiel",
			"content": "Sed soluta nemo et consectetur reprehenderit ea reprehenderit sit.",
			"tags": [
			  "organization",
			  "planning",
			  "collaboration"
			],
			"created_at": "2021-03-20T11:34:12.000000Z",
			"updated_at": "2021-03-20T11:34:12.000000Z"
		  },
		  "message": "See post"
		}
``` 
    
## Editando um post
`PUT /api/posts/{id}

### Request

   **Headers**
   
    Authentication: Bearer JWT
    Accept: application/json
    Content-Type: application/json
    
   **Body**
   
```js
    {
      "author": "Marcia Thiel",
      "title": "Notion",
      "content": "Sed soluta nemo et consectetur reprehenderit ea reprehenderit sit.",
      "tags": [1,2,3]
    }
```

### Response

   **Headers**
   
        Content-Type: application/json
   **Body**
   
```js
		{
		  "success": true,
		  "data": {
			"id": 32,
			"title": "Notion",
			"author": "Marcia Thiel",
			"content": "Sed soluta nemo et consectetur reprehenderit ea reprehenderit sit.",
			"tags": [
			  "organization",
			  "planning",
			  "collaboration"
			],
			"created_at": "2021-03-20T11:34:12.000000Z",
			"updated_at": "2021-03-20T11:34:12.000000Z"
		  },
		  "message": "See post"
		}
``` 
        
## Deletando um post
`DELETE /api/posts/{id}`

   **Headers**
   
    Authentication: Bearer JWT
    Content-Type: application/json
    
### Response

   **Headers**
   
        Content-Type: application/json
   **Body**
          
          No content
        