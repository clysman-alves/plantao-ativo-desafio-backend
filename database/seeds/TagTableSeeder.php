<?php

use App\Models\Tag;
use Illuminate\Database\Seeder;

class TagTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tags = [
            "organization","planning","collaboration", "writing",
            "calendar", "api","json", "schema","node",
            "github", "rest", "web","framework",
            "node", "http2", "https", "localhost"
        ];

        foreach ($tags as $key => $t) {
            Tag::create([
                "title" => $t
            ]);
        }
    }
}
