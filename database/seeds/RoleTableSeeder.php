<?php

use Illuminate\Database\Seeder;
use App\Models\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            (object) array(
                "name" => "admin",
                "label" => "Administrador",
                "permissions" => []
            ),
            (object) array(
                "name" => "editor",
                "label" => "Editor",
                "permissions" => [1, 2]

            ),
            (object) array(
                "name" => "manager",
                "label" => "Manager",
                "permissions" => [1]
            )
        ];

        foreach ($roles as $key => $r) {
            $role = Role::create([
                "name" => $r->name,
                "label" => $r->label,
            ]);

            $role->permissions()->sync($r->permissions);
        }
    }
}
