<?php

use Illuminate\Database\Seeder;
use App\Models\Permission;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            array(
                "name" => "VIEW_POST",
                "label" => "Visualizar Postagem"
            ),
            array(
                "name" => "EDIT_POST",
                "label" => "Editar Postagem"
            ),
            array(
                "name" => "DELETE_POST",
                "label" => "Deletar Postagem"
            ),
        ];

        foreach ($permissions as $key => $p) {
            $permission = Permission::create($p);
        }
    }
}
