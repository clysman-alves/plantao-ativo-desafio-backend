<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Post;
use Faker\Generator as Faker;

$factory->define(Post::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence,
        'content' => $faker->paragraph,
        'user_id' => \App\Models\User::all('id')->random(),
    ];
});

$factory->afterCreating(Post::class, function ($post, $faker) {
    $post->tags()->sync([
        $faker->randomDigitNotNull,
        $faker->randomDigitNotNull,
        $faker->randomDigitNotNull
    ]);
});
